# README #

Simple script to display battery alerts at a custom level.


### How do I get set up? ###

User has to specify as a parameter the percentage level below which the battery alerts should pop up.
Pull code, setup a `crontab` entry for it, using the preferred interval of checking.  

Example `crontab` entry, popping up alerts each 3 minutes below a battery level of 15% :

    */3 * * * * /home/user/macbatt/macbatt.sh 15